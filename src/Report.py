import datetime
import numpy
import re

from typing import Dict, Set, Optional, List, Tuple
from pathlib import PureWindowsPath
from urllib.parse import urlparse

from src.utility import normalize_dict_in_set, set_none_empty_obj_fields_and_string_set_tolower
from src.markovrandomness.RandomnessMarkovChain import RandomnessMarkovChain, load_model


IP_REGEX = re.compile(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$')


class HeaderMeta:
    size: int
    entrypoint: Optional[int]
    entrypoint_section: Optional[str]  # categorical
    subsystem: Optional[str]  # categorical
    timestamp: Optional[int]
    timestamp_year: Optional[int]
    overlay_data: bool

    def __init__(self, r: Dict, ai: Dict):
        self.size = r['size']
        self.overlay_data = 'overlay' in r['tags']
        self.entrypoint = ai.get('pe-entry-point')
        self.timestamp = ai.get('pe-timestamp')
        et = ai.get('exiftool')
        if et is not None:
            if self.timestamp is None:
                et_timestamp = et.get('TimeStamp')
                if et_timestamp == '0000:00:00 00:00:00':
                    self.timestamp = 0
                else:
                    try:
                        self.timestamp = int(datetime.datetime.strptime(et_timestamp, '%Y:%m:%d %H:%M:%S%z').timestamp())
                    except ValueError:
                        self.timestamp = None
            self.subsystem = et.get('Subsystem')
            ep = et.get('EntryPoint')
            if ep is not None:
                if self.entrypoint is None:
                    self.entrypoint = int(et['EntryPoint'], 16)
                else:
                    assert self.entrypoint == int(et['EntryPoint'], 16)
            if self.subsystem is not None:
                if 'Unknown' in self.subsystem:
                    self.subsystem = None
                if self.subsystem == 'Windows CE GUI':
                    self.subsystem = 'Windows GUI'
        else:
            self.subsystem = None
            self.timestamp = None
        if self.timestamp is not None and self.timestamp != 0:
            self.timestamp_year = datetime.datetime.fromtimestamp(self.timestamp).year
        assert self.subsystem in ['Windows GUI', 'Windows command line', 'Native', None]
        self.entrypoint_section = None
        if self.entrypoint is not None and 'sections' in ai:
            for s in ai['sections']:
                s_vaddr = int(s[1])
                s_vsize = int(s[2])
                if s_vaddr <= self.entrypoint <= s_vaddr+s_vsize:
                    self.entrypoint_section = s[0]


class Signature:
    verified: Optional[bool]
    signers: Optional[str]  # categorical

    def __init__(self, signature_report: Dict):
        v = signature_report.get('verified')
        if v is None:
            self.verified = None
        else:
            self.verified = v == 'Signed'
        self.signers = signature_report.get('signers')
        if self.signers is not None:
            self.signers = self.signers.split(';')[0].split(' ')[0]


class Section:
    name: str  # categorical
    rawsize: int  # min, max, svg, std, med
    entropy: float  # min, max, svg, std, med

    def __init__(self, section: List):
        self.name = section[0]
        self.rawsize = int(section[3])
        self.entropy = float(section[4])


class Resource:
    type: str  # categorical
    entropy: Optional[float]  # min, max, svg, std, med

    def __init__(self, resource: Dict):
        self.type = resource['type']  # categorical
        self.entropy = resource.get('entropy')


class Strings:
    tot_domains: int
    domains_randomness_score: Optional[Set[float]]
    tot_ips: int
    tot_urls: int
    urls: Optional[Set[Tuple[str, Optional[float], int, float]]]

    def __init__(self, ai: Dict):
        self.domains = set()
        self.ips = set()
        self.urls = set()

        embedded_domains = ai.get('embedded_domains')
        if embedded_domains is not None:
            self.domains_randomness_score = {load_model().get_score(d) for d in embedded_domains}
            self.tot_domains = len(self.domains_randomness_score)
        else:
            self.domains_randomness_score = None
            self.tot_domains = 0
        embedded_ips = ai.get('embedded_ips')
        if embedded_ips is not None:
            self.tot_ips = len(embedded_ips)
        else:
            self.tot_ips = 0
        embedded_urls = ai.get('embedded_urls')
        if embedded_urls is not None:
            self.urls = {Network.dissect_url(url) for url in embedded_urls}
            self.tot_urls = len(self.urls)
        else:
            self.urls = None
            self.tot_urls = 0


class Network:
    domains: Set
    urls: Set[Tuple[str, Optional[float], int, float]]
    len_udp: int
    udp_ports: Optional[Set[int]]  # categorical (even if they are integers)
    len_tcp: int
    tcp_ports: Optional[Set[int]]  # categorical (even if they are integers)
    len_dns: int
    http_useragents: Optional[Set[str]]  # categorical

    def __init__(self, network_report: Dict):
        self.udp_ports = set(int(addr_port.split(':')[-1]) for addr_port in network_report['udp'])
        self.len_udp = len(self.udp_ports)
        self.tcp_ports = set(int(addr_port.split(':')[-1]) for addr_port in network_report['tcp'])
        self.len_tcp = len(self.tcp_ports)
        dns = {e['hostname'] for e in network_report['dns']}
        self.len_dns = len(dns)
        self.domains = set()
        for d in dns:
            dspt = d.split('.')
            if len(dspt) > 1:
                self.domains.add(f'{dspt[-2]}.{dspt[-1]}')
        self.urls = set()
        self.http_useragents = set()
        for e in network_report['http']:
            self.urls.add(Network.dissect_url(e['url']))
            ua = e.get('user-agent')
            if ua is not None:
                ua = ua.split(' ')[0]
                if len(ua) <= 1:
                    ua = None
                elif '/' in ua:
                    ua = ua.split('/')[0]
            if ua is not None:
                self.http_useragents.add(ua.lower())

    @staticmethod  # (scheme, domain_randomness_score, port, resource_path_randomness_score)
    def dissect_url(u) -> Tuple[str, Optional[float], int, float]:
        up = urlparse(u)
        netloc = up.netloc
        if ':' in netloc:
            nsplt = netloc.split(':')
            domain = nsplt[0]
            port = re.sub(r'[^0-9]', '', nsplt[1])
            if port == '':
                port = 80
            else:
                port = int(port)
        else:
            domain = netloc
            port = 80
        res_path = up.path.split('/')[-1].lower()
        if IP_REGEX.match(domain):
            domain = None  # if domain_randomness_score is None: it uses and IP address
        else:
            domain = load_model().get_score(domain)
        return up.scheme, domain, port, load_model().get_score(res_path)


class Processes:
    len_injected: int
    injected_exe: Optional[Set[str]]  # categorical
    len_terminated: int
    terminated_exe: Optional[Set[str]]  # categorical
    len_created: int
    created: Optional[Set[Tuple]]  # (parent_folder, is_temp_folder?, extension, randomness_score)
    len_shellcmds: int
    shellcmds: Optional[Set[Tuple]]  # (parent_folder, is_temp_folder?, extension, randomness_score)

    def __init__(self, processes_report: Dict):
        self.len_injected = self.len_terminated = self.len_created = self.len_shellcmds = 0
        for k, v in processes_report.items():
            if k == 'injected':
                injected = {e['proc'].lower() for e in v}
                self.len_injected = len(injected)
                self.injected_exe = {e for e in injected if '.exe' in e}
                #if self.injectedexe: print(self.injectedexe)
            elif k == 'created':
                self.created = {
                    # removed arguments
                    FileSystem.extract_path_components(e['proc'].replace('"', ' ').split(' ')[0]) for e in v}
                self.len_created = len(self.created)
            elif k == 'terminated':
                terminated = {e['proc'].lower() for e in v}
                self.len_terminated = len(terminated)
                self.terminated_exe = {e for e in terminated if '.exe' in e}
            elif k == 'shellcmds':
                self.shellcmds = {FileSystem.extract_path_components(
                    re.sub(
                        r'\(?\(?(null|open|explore|runas)\)?\)?|"|-|\[|\]|/', ' ',
                        e['cmd']).strip().split()[0]) for e in v if '(null)' not in e['cmd']}
                self.len_shellcmds = len(self.shellcmds)


class Registry:
    deleted: Optional[Set[str]]
    set: Optional[Set[str]]

    def __init__(self, registry_report: Dict):
        self.deleted = {e['key'] for e in registry_report['deleted'] if e['key'].startswith('H')}
        self.set = {e['key'] for e in registry_report['set'] if e['key'].startswith('H')}


class FileSystem:
    read: Optional[Set[Tuple]]
    written: Optional[Set[Tuple]]
    deleted: Optional[Set[Tuple]]

    def __init__(self, fs_report: Dict):
        self.read = set()
        self.written = set()
        self.deleted = set()
        for k, v in fs_report.items():
            if k == 'opened' or k == 'read':
                self.read.update({FileSystem.extract_path_components(e['path']) for e in v})
            elif k == 'written' or k == 'downloaded':
                self.written.update({FileSystem.extract_path_components(e['path']) for e in v})
            elif k == 'deleted':
                self.deleted.update({FileSystem.extract_path_components(e['path']) for e in v})
            elif k == 'moved':
                for e in v:
                    self.deleted.add(FileSystem.extract_path_components(e['src']))
                    self.written.add(FileSystem.extract_path_components(e['dst']))
            elif k == 'copied':
                for e in v:
                    self.read.add(FileSystem.extract_path_components(e['src']))
                    self.written.add(FileSystem.extract_path_components(e['dst']))
            elif k == 'replaced':
                for e in v:
                    self.deleted.add(FileSystem.extract_path_components(e['replaced']))
                    self.written.add(FileSystem.extract_path_components(e['replacement']))

    @staticmethod  # (parent_folder, is_temp_folder?, extension, randomness_score)
    def extract_path_components(p: str) -> Tuple[str, bool, str, float]:
        pwp = PureWindowsPath(p.lower())
        name_without_suffix = pwp.name.replace(' ', '').replace(pwp.suffix, '')
        randomness_score = load_model().get_score(name_without_suffix)
        return str(pwp.parent), 'temp' in pwp.parts, pwp.suffix, randomness_score


class RuntimeDLLs:
    len_default: int
    len_system: int
    len_custom: int
    dll_name_randomness_scores: Set[float]  # min, max, svg, std, med

    def __init__(self, dyndll_report: Dict):
        self.len_default = self.len_system = self.len_custom = 0
        self.dll_name_randomness_scores = set()
        for rtdll in dyndll_report:
            dll: str = rtdll['file'].lower()
            if '\\' in dll:
                if dll.startswith("c:\\windows\\"):
                    self.len_system += 1
                else:
                    self.len_custom += 1
                dll_name = PureWindowsPath(dll).name.replace('.dll', '')
            else:
                self.len_default += 1
                dll_name = dll.replace('.dll', '')
            self.dll_name_randomness_scores.add(load_model().get_score(dll_name))


class Report:
    sha256: str  # ID, not a feature
    is_malware: bool
    signature: Optional[Signature]
    header_meta: HeaderMeta
    sections: Optional[List[Section]]
    imports: Optional[Set[str]]
    resources: Optional[List[Resource]]
    strings: Strings
    processes: Processes
    services: Optional[Set[str]]
    tot_services: int
    services_names_randomness: Optional[Set[float]]
    registry: Optional[Registry]
    mutexes: Optional[Set[str]]
    tot_mutexes: int
    mutexes_names_randomness: Optional[Set[float]]
    file_system: FileSystem
    runtime_dlls: RuntimeDLLs

    def __init__(self, vt_report: Dict):
        self.sha256 = vt_report['sha256']
        self.is_malware = int(vt_report['positives']) > 16
        ai: Dict = vt_report['additional_info']
        b: Dict = ai['behaviour-v1']

        # Signature
        if 'sigcheck' in ai:
            self.signature = Signature(ai['sigcheck'])
            if self.signature.signers is None and self.signature.verified is None:
                self.signature = None
        else:
            self.signature = None
        # Header Metadata
        self.header_meta = HeaderMeta(vt_report, ai)
        # Sections
        if 'sections' in ai:
            self.sections = [Section(s) for s in ai['sections']]
        else:
            self.sections = None
        # Imports
        imports = ai.get('imports')
        if imports is None:
            self.imports = None
        else:
            self.imports = set()
            for k, v in imports.items():
                for e in v:
                    if 'Ord(' not in e:  # removes dirty data
                        self.imports.add(f'{k}{e}')
        # Resources
        if 'pe-resource-detail' in ai:
            self.resources = [Resource(r) for r in ai['pe-resource-detail']]
        else:
            self.resources = None
        # Strings
        self.strings = Strings(ai)
        # Network
        self.network = Network(b['network'])
        # Processes
        self.processes = Processes(b['process'])
        # Services
        self.services = normalize_dict_in_set(b['service'], 'name')
        if self.services is None:
            self.tot_services = 0
            self.services_names_randomness = None
        else:
            self.tot_services = len(self.services)
            self.services_names_randomness = {load_model().get_score(s) for s in self.services}
        # Registry
        self.registry = Registry(b['registry'])
        set_none_empty_obj_fields_and_string_set_tolower(self.registry)
        # Mutexes
        self.mutexes = normalize_dict_in_set(b['mutex'], 'mutex')
        if self.mutexes is None:
            self.tot_mutexes = 0
            self.mutexes_names_randomness = None
        else:
            self.tot_mutexes = len(self.mutexes)
            self.mutexes_names_randomness = {load_model().get_score(m) for m in self.mutexes}
        # File System
        self.file_system = FileSystem(b['filesystem'])
        # Runtime DLLs
        self.runtime_dlls = RuntimeDLLs(b['runtime-dlls'])
        set_none_empty_obj_fields_and_string_set_tolower(self)

    def vectorize(self) -> numpy.ndarray:
        # Signature
        # Header Metadata
        # Sections -- stats about fields + section's name
        # Imports
        # Resources -- stats about fields + resource's type
        # Strings
        # Network -- just interested in len() of fields
        # Processes -- just interested in len() of fields
        # Services
        # Registry
        # Mutexes
        # File System -- interested in len() of fields but also the path
        # Runtime DLLs -- just interested in len() of fields
        raise NotImplementedError()
