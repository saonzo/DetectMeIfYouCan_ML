import numpy as np

from typing import List, Optional, Tuple
from collections import Counter
from os.path import isdir, join, abspath, dirname, isfile
from pathlib import Path

from src.utility import os_listdir_json, read_json


def avclass_distrib(avclass_labels):
    assert isfile(avclass_labels)
    print('> AVClass labels distrib:')
    with open(avclass_labels) as fp:
        labels = [l.split('\t')[1].strip() for l in fp.readlines() if 'SINGLETON' not in l]
        print(Counter(labels))


def all_stats(lst: List) -> Optional[Tuple]:
    len_lst = len(lst)
    if len_lst <= 0:
        return None
    return np.min(lst), np.max(lst), np.mean(lst), np.std(lst), np.median(lst)


def pe_filter(pe_magic: str) -> bool:
    return pe_magic.startswith('PE32') and not any(x in pe_magic for x in ['DLL', '.Net', 'Installer', 'ARM'])


def dataset_checks(tgt_folder: str):
    is_malware = 'malware' in tgt_folder
    if not is_malware:
        assert 'goodware' in tgt_folder
        print('> Goodware stats:')
    else:
        print('> Malware stats:')
    detections = list()
    years = list()
    for j in os_listdir_json(tgt_folder):
        report = read_json(j)
        ai = report['additional_info']
        positives = report['positives']
        detections.append(positives)
        if is_malware:
            assert positives >= 21
        else:
            assert positives <= 3
        assert ai.get('behaviour-v1') is not None
        assert pe_filter(ai['magic'])

        first_seen = report.get('first_seen')
        if first_seen is None:
            scan_date = report['scan_date']
            first_seen_year = scan_date.split('-')[0]
        else:
            first_seen_year = first_seen.split('-')[0]
        years.append(int(first_seen_year))

    print('>> Detections:')
    print(all_stats(detections))
    print(Counter(detections))
    print()
    print('>> Years:')
    print(all_stats(years))
    print(Counter(years))
    print()


if __name__ == "__main__":
    basedir = abspath(dirname(__file__))
    dataset_folder = join(Path(basedir).parent, 'dataset')
    assert isdir(dataset_folder)
    malware_folder = join(dataset_folder, 'malware')
    goodware_folder = join(dataset_folder, 'goodware')

    avclass_distrib(join(dataset_folder, 'avclass.labels'))
    for dir in [goodware_folder, malware_folder]: dataset_checks(dir)
