import json
import sys
import time

from multiprocessing import Pool
from os.path import isdir, join, abspath, dirname, isfile
from pathlib import Path
from typing import List, Optional, Tuple

from src.Report import Report
from src.utility import os_listdir_json, read_json
from src.markovrandomness.RandomnessMarkovChain import RandomnessMarkovChain, load_model
from itertools import repeat

basedir = abspath(dirname(__file__))
dataset_folder = join(Path(basedir).parent, 'dataset')
assert isdir(dataset_folder)
malware_folder = join(dataset_folder, 'malware')
goodware_folder = join(dataset_folder, 'goodware')
game_folder = join(dataset_folder, 'game')


RND_MC: RandomnessMarkovChain = load_model()  # this is trick for multiprocess
assert RND_MC is not None


def load_report(json_file: str) -> Report:
    return Report(read_json(json_file))


def load_dataset() -> Tuple[List[Report], List[Report], List[Report]]:
    with Pool() as pool:
        start_time = time.time()

        malware = pool.map(load_report, os_listdir_json(malware_folder))
        for m in malware:
            assert m.is_malware

        goodware = pool.map(load_report, os_listdir_json(goodware_folder))
        for g in goodware:
            assert not g.is_malware

        len_goodware = len(goodware)
        len_malware = len(malware)
        assert len_goodware == len_malware

        game = pool.map(load_report, os_listdir_json(game_folder))
        assert len(game) == 20
        gm = gg = i = 0
        for g in game:
            i += 1
            print(i, g.sha256)
            if g.is_malware:
                gm += 1
            else:
                gg += 1
        assert gm == 11 and gg == 9
        print(f'> Dataset loaded in {round(time.time() - start_time, 1)}sec')
        print(f'#malware={len_malware} #goodware={len_goodware} #game={len(game)}')
    return malware, goodware, game


def main():
    malware, goodware, game = load_dataset()
    #sys.exit()
    all = list()
    for reports in [malware, goodware]:
        for r in reports:
            r: Report
            '''
            all.extend(r.network.http_useragents)
            if r.registry.set is not None:
                print(r.registry.set)
            if r.registry.deleted is not None:
                print(r.registry.deleted)
            
            if r.header_meta.timestamp is not None:
                print(r.header_meta.timestamp)
                all.add(r.header_meta.timestamp)
            '''
    from collections import Counter
    print(Counter(all))
    print((len(all)))


if __name__ == "__main__":
    len_sys_argv = len(sys.argv)
    if len_sys_argv <= 1:
        main()
        sys.exit()
    elif len_sys_argv == 2:
        sys_argv1 = sys.argv[1]
        if isfile(sys_argv1):
            if sys_argv1.endswith('json'):
                report = load_report(sys_argv1)
            elif sys_argv1.endswith('jsons'):
                reports = list()
                with open(sys_argv1, 'r') as fp:
                    for line in fp:
                        try:
                            reports.append(Report(json.loads(line)))
                        except KeyError:
                            pass
                print(len(reports))
        elif isdir(sys_argv1):
            reports = list()
            for f in os_listdir_json(sys_argv1):
                try:
                    reports.append(load_report(f))
                except KeyError:
                    pass
            print(len(reports))
        breakpoint()
