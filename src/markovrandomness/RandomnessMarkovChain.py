from os.path import abspath, dirname, isfile, isdir, join

from networkx import DiGraph, MultiDiGraph
from typing import List, Tuple, Optional
from collections import Counter
import numpy as np
from src.utility import load_pickle
import pickle
import string
import secrets
import math
import sys


class RandomnessMarkovChain:

    def __init__(self):
        self.markov_chain_graph = None
        self.multi_di_graph = MultiDiGraph()

    def add_training_string(self, s: str):
        s = s.lower()
        for i in range(len(s) - 1):
            self.multi_di_graph.add_edge(s[i], s[i + 1])

    def force_build_model(self) -> DiGraph:
        self.markov_chain_graph = None
        return self.build_model()

    def build_model(self) -> DiGraph:
        if self.markov_chain_graph is not None:
            return self.markov_chain_graph
        self.markov_chain_graph = DiGraph()
        for n in self.multi_di_graph.nodes():
            out_edges_n = self.multi_di_graph.out_edges(n)
            len_out_edges_n = len(out_edges_n)
            out_nodes_list = [oe[1] for oe in out_edges_n]
            if len_out_edges_n > 0:
                perc_counter = dict()
                for k, v in Counter(out_nodes_list).items():
                    perc_counter[k] = v / len_out_edges_n
                assert 0.9 <= sum(perc_counter.values()) <= 1.1
                for k, v in perc_counter.items():
                    self.markov_chain_graph.add_edge(n, k, weight=v)
        return self.markov_chain_graph

    def get_str_probwalk(self, s: str) -> List[float]:
        assert len(s) > 1
        s = s.lower()
        model: DiGraph = self.build_model()
        probwalk = list()
        for i in range(len(s) - 1):
            try:
                w = model[s[i]][s[i + 1]]['weight']
                assert 0.0 <= w <= 1.0
                probwalk.append(w)
            except KeyError:
                probwalk.append(0.0)
        return probwalk

    def get_score(self, s: str) -> Optional[float]:
        len_s = len(s)
        if len_s == 0:
            return None
        if len_s == 1:
            return 0.0
        if len_s > 64:
            s = s[:64]
        log_sum = sum([math.log(n) for n in self.get_str_probwalk(s) if n > 0.0])
        if log_sum == 0.0:
            return 0.0
        prob_score = math.exp(log_sum)
        assert 0.0 < prob_score < 1.0
        return prob_score

    def save_model(self, file_name: str = 'mcmodel.pickle'):
        assert self.build_model() is not None
        pickle.dump(self, open(file_name, 'wb'))


def create_model():
    mc = RandomnessMarkovChain()
    win_names = load_pickle('names_set.pickle')
    for n in win_names:
        mc.add_training_string(n)

    with open("words_alpha.txt") as input:
        words = [line.strip() for line in input.read().splitlines()]

    for w in words:
        mc.add_training_string(w)
    mc.save_model()


def generate_random_string(l: int) -> str:
    secrets_gen = secrets.SystemRandom()
    alphabet = string.ascii_letters
    rnd_str = ''.join(secrets.choice(alphabet) for i in range(l))
    return rnd_str


RND_MC = None


def load_model() -> RandomnessMarkovChain:
    global RND_MC
    if RND_MC is None:
        rnd_mc_file = join(abspath(dirname(__file__)), 'mcmodel.pickle')
        if not isfile(rnd_mc_file):
            import requests
            rnd_mc_file_url = 'https://www.dropbox.com/s/qgwwpudcxtj9qno/mcmodel.pickle?dl=1'
            print(f"> File '{rnd_mc_file}' not found! Downloading...")
            r = requests.get(rnd_mc_file_url, allow_redirects=True)
            with open(rnd_mc_file, 'wb') as fd:
                fd.write(r.content)
            assert isfile(rnd_mc_file)
            print("> Download completed.")
        RND_MC = load_pickle(rnd_mc_file)
    return RND_MC


if __name__ == '__main__':
    #create_model(); sys.exit()
    mc = load_model()
    test_strings = [
        'simone',
        'alessandro',
        'davide',
        'yufei',
        'paper',
        'phone2347',
        'ddns',
    ]

    test_strings.extend([generate_random_string(len(s)) for s in test_strings])
    for s in test_strings:
        print(f"'{s}'", mc.get_score(s))
