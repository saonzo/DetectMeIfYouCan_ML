#!/usr/bin/python3

import sys
import re
import os
import pickle
from os.path import isdir, isfile, join
from pathlib import Path


def get_all_names():
    all_names = set()
    start_folder = '/mnt/'
    for root, dirs, files in os.walk(start_folder, topdown=False):
        print('>', root)
        for f in files:
            p = Path(f)
            all_names.add(p.name.replace(p.suffix, ''))
    print('> DONE #=', len(all_names))
    with open('names_set.pickle', 'wb') as fp:
        pickle.dump(all_names, fp)


if __name__ == '__main__':
    #get_all_names(); sys.exit()
    with open('names_set.pickle', 'rb') as fp:
        names = pickle.load(fp)
        print(len(names))
