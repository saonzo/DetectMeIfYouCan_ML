import os
import pickle
import json
from os.path import isdir, abspath, join, isfile
from typing import List, Dict, Optional, Set


def os_listdir_json(folder: str) -> List[str]:
    ret = [abspath(join(folder, f)) for f in os.listdir(folder) if f.endswith('.json')]
    ret.sort()
    return ret


def read_json(file_path: str) -> Dict:
    with open(file_path, 'r', encoding='utf-8') as in_file:
        return json.load(in_file)


def normalize_dict_in_set(diz: Dict, key_name: str) -> Optional[Set[str]]:
    out = set()
    for k, v in diz.items():
        if isinstance(v, list):
            for e in v:
                if isinstance(e, str):
                    out.add(e)
                elif isinstance(e, dict):
                    if key_name in e:
                        out.add(e[key_name])
        elif isinstance(v, dict):
            for kk, vv in v.items():
                out.add(vv[key_name])
    if len(out) <= 0:
        return None
    return {e.lower() for e in out}


def set_none_empty_obj_fields_and_string_set_tolower(obj):
    for k, v in obj.__dict__.items():
        if isinstance(v, set):
            if len(v) <= 0:
                obj.__dict__[k] = None
            else:
                if all(isinstance(e, str) for e in obj.__dict__[k]):
                    obj.__dict__[k] = {s.lower() for s in obj.__dict__[k]}


def load_pickle(fname: str):
    assert isfile(fname)
    return pickle.load(open(fname, 'rb'))


def save_pickle(fname: str, data):
    pickle.dump(data, open(fname, 'wb'))
