import numpy as np 

feature_name_list = []
with open('feature_name_3percent.txt','r') as f:
     all_names = f.readlines(1000000000000)
     for k in range(len(all_names)):
         feature_name_list.append(all_names[k])

unique_names = np.unique(np.array(feature_name_list))
freq_names = []
for k in range(len(unique_names)):
    freq_names.append(len(np.where(np.array(feature_name_list) == unique_names[k])[0]))


idx = np.argsort(-1*np.array(freq_names))
with open('freq_feature_name_3percent.txt','w') as f:
     for k in range(len(idx)):
         str_name = unique_names[idx[k]][:-1] + ' : ' + str(freq_names[idx[k]])
         str_name += '\n'
         f.write(str_name)

