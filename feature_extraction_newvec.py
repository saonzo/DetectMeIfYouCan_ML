import sys
import time
import numpy as np 

from multiprocessing import Pool
from os.path import isdir, join, abspath, dirname
from pathlib import Path
from typing import List, Optional, Tuple

from src.Report import Report
from src.utility import os_listdir_json, read_json
from src.markovrandomness.RandomnessMarkovChain import RandomnessMarkovChain, load_model
from itertools import repeat


from sklearn.metrics import roc_auc_score
from sklearn.ensemble import RandomForestClassifier


#basedir = abspath(dirname(__file__))
#dataset_folder = join(Path(basedir).parent, 'dataset')
#assert isdir(dataset_folder)
malware_folder = 'dataset/malware'#join(dataset_folder, 'malware')
goodware_folder = 'dataset/goodware'#join(dataset_folder, 'goodware')
#game_folder = join(dataset_folder, 'game')

RND_MC: RandomnessMarkovChain = load_model()  # this is trick for multiprocess
assert RND_MC is not None

def load_report(json_file: str) -> Report:
    return Report(read_json(json_file))


def feature_extract_strings(strfeature,unique_str):
    numfeature = np.zeros((len(unique_str),))
    for i in range(len(unique_str)):
        if unique_str[i] in strfeature:
            numfeature[i] = 1.

    return numfeature 


def unique_name_section(section_strings):
    names = list()
    for i in range(len(section_strings)):
        names.extend(section_strings[i][-1])

    return np.unique(np.array(names))

def unique_subsystem(header_meta_strings):
    subsys = list()
    for i in range(len(header_meta_strings)):
        subsys.append(header_meta_strings[i][1])

    return np.unique(np.array(subsys))

def flatten_dict(resource_dict):
    resource_types = []
    for i in range(len(resource_dict)):
        resource_types.extend(list(resource_dict[i].keys()))

    return np.unique(np.array(resource_types))


def flatten_list(string_list):
    flat_strings = []
    for sublist in string_list:
        for single_string in sublist:
            flat_strings.append(single_string)

    return np.unique(np.array(flat_strings))



def FileSystemExtraction(report_class):

    #'c:\\windows\\registration', False, '.clb', 1.6064915895019597e-12
    #### directory: categorical feature 
    #### percentage of tmp dirs 
    #### extension: categorical feature 
    #### min, max, mean, median, std of filename randomness 
    read_dir_name = []
    read_tmp_pt = 0.
    read_ext_name = []
    read_rand_stat = []
    len_read = 0

    if report_class.file_system.read is None:
        read_dir_name.append('None')
        read_tmp_pt = -1.
        read_ext_name.append('None')
        read_rand_stat = [-1,-1,-1,-1,-1]
        len_read = 0.
    else:
        read_strings = list(report_class.file_system.read)
        len_read = len(read_strings)
        random_score = []
        for k in range(len(read_strings)):
            read_dir_name.append(read_strings[k][0])
            if read_strings[k][1] is True:
                read_tmp_pt += 1./float(len(read_strings))
            read_ext_name.append(read_strings[k][2])
            if read_strings[k][3] is None:
                continue
            else:
                random_score.append(read_strings[k][3])
        #print(random_score)
        if len(random_score) >0:
            read_rand_stat = [np.min(random_score),np.max(random_score),np.mean(random_score),np.median(random_score),np.std(random_score)]
        else:
            read_rand_stat = [-1,-1,-1,-1,-1]

        

    written_dir_name = []
    written_tmp_pt = 0.
    written_ext_name = []
    written_rand_stat = []
    len_written = 0.

    if report_class.file_system.written is None:
        written_dir_name.append('None')
        written_tmp_pt = -1.
        written_ext_name.append('None')
        written_rand_stat = [-1,-1,-1,-1,-1]
        len_written = 0.
    else:
        written_strings = list(report_class.file_system.written)
        len_written = len(written_strings)
        random_score = []
        for k in range(len(written_strings)):
            written_dir_name.append(written_strings[k][0])
            if written_strings[k][1] is True:
                written_tmp_pt += 1./float(len(written_strings))
            written_ext_name.append(written_strings[k][2])
            if written_strings[k][3] is None:
                continue
            else:
                random_score.append(written_strings[k][3])

        if len(random_score) >0:
            written_rand_stat = [np.min(random_score),np.max(random_score),np.mean(random_score),np.median(random_score),np.std(random_score)]
        else:
            written_rand_stat = [-1,-1,-1,-1,-1]


    deleted_dir_name = []
    deleted_tmp_pt = 0.
    deleted_ext_name = []
    deleted_rand_stat = []
    len_deleted = 0.

    if report_class.file_system.deleted is None:
        deleted_dir_name.append('None')
        deleted_tmp_pt = -1.
        deleted_ext_name.append('None')
        deleted_rand_stat = [-1,-1,-1,-1,-1]
        len_deleted = 0.
    else:
        deleted_strings = list(report_class.file_system.deleted)
        len_deleted = len(deleted_strings)
        random_score = []
        for k in range(len(deleted_strings)):
            deleted_dir_name.append(deleted_strings[k][0])
            if deleted_strings[k][1] is True:
                deleted_tmp_pt += 1./float(len(deleted_strings))
            deleted_ext_name.append(deleted_strings[k][2])
            if deleted_strings[k][3] is None:
                continue
            else:
                random_score.append(deleted_strings[k][3])

        if len(random_score) >0:
            deleted_rand_stat = [np.min(random_score),np.max(random_score),np.mean(random_score),np.median(random_score),np.std(random_score)]
        else:
            deleted_rand_stat = [-1,-1,-1,-1,-1]


    return [read_dir_name,read_rand_stat,len_read,written_dir_name,written_rand_stat,len_written,deleted_dir_name,deleted_rand_stat,len_deleted]


def SignatureExtraction(report_class):
    signer = ''
    if report_class.signature is None:
        signer = 'Signer: None'
    else:
        signer += str(report_class.signature.signers) 
        if report_class.signature.verified is True:
            signer += '; ' + 'True'
        else:
            signer += '; ' + 'False'
        #signer += '; ' + report_class.signature.verified

    return signer 


def HeaderMetaExtraction(report_class):
    hm = report_class.header_meta
    if hm.entrypoint is None:
        ep = -1
    else:
        ep = hm.entrypoint

    if hm.entrypoint_section is None:
        epsec = 'HeaderMeta Entrypoint: None'
    else:
        epsec = hm.entrypoint_section
    
    if hm.subsystem is None:
        sub = 'HeaderMeta Subsystem: None'
    else:
        sub = hm.subsystem
    
    if hm.timestamp is None:
        ts_int = -1
    else:
        ts_int = hm.timestamp

    #if hm.timestamp_year is None:
    #    ts_year = -1 
    #else:
    #    ts_year = hm.timestamp_year

    hm_size = hm.size 
    hm_od = hm.overlay_data  
 

    return [ep, epsec, sub, ts_int, hm_size, hm_od]


def SectionExtraction(report_class):
    sections = report_class.sections
    vsize = []
    entropy = []
    name = []

    if sections is None:
        vsize = -1
        entropy = -1
        name.append('sections: None')
        return [vsize,vsize,vsize,vsize,vsize,entropy,entropy,entropy,entropy,entropy,name]

    for k in range(len(sections)):
        vsize.append(sections[k].rawsize)
        entropy.append(sections[k].entropy)
        name.append(sections[k].name)

    return [np.min(vsize),np.max(vsize),np.median(vsize),np.mean(vsize),np.std(vsize),np.min(entropy),np.max(entropy),np.median(entropy),np.mean(entropy),np.std(entropy),np.unique(np.array(name)).tolist()]

def ImportsExtraction(report_class):
    if report_class.imports is None:
        import_strings = []
        import_strings.append('Import: None')
    else:
        import_strings = list(report_class.imports)
    
    return import_strings 

def ResourcesExtraction(report_class):
    resource_strings = []
    resource_entropy = []
    if report_class.resources is None:
        resource_strings.append('Resource: None')
        return [-1,-1,-1,-1,-1,resource_strings]
    else:
        resources = report_class.resources
        for k in range(len(resources)):
            resource_strings.append(resources[k].type)
            if resources[k].entropy is None:
                continue
            else:
                resource_entropy.append(resources[k].entropy)

        if len(resource_entropy) == 0:
            min_score = -1 
            max_score = -1 
            mean_score = -1 
            median_score = -1
            std_score = -1 
        else:
            min_score = np.min(resource_entropy)
            max_score = np.max(resource_entropy)
            mean_score = np.mean(resource_entropy)
            median_score = np.median(resource_entropy)
            std_score = np.std(resource_entropy)

    return [min_score,max_score,mean_score,median_score,std_score,resource_strings]


    

def StringsExtraction(report_class):
    result_strings = []
    result_strings.append(report_class.strings.tot_domains)
    result_strings.append(report_class.strings.tot_ips)
    result_strings.append(report_class.strings.tot_urls)

    if report_class.strings.urls is None:
        url_scheme = ['No URL']
        url_domain = [-1.0]
        url_port = [-1.0] 
        url_score = [-1.0] 
        result_strings.append([url_scheme,url_domain,url_port,url_score])
    else:
        url_list = list(report_class.strings.urls)
        url_scheme = []
        url_domain = []
        url_port = []
        url_score = []
        for k in range(len(url_list)):
            url_scheme.append(url_list[k][0])
            if url_list[k][1] is None:
                url_domain.append(-1.)
            else:
                url_domain.append(url_list[k][1])

            url_port.append(url_list[k][2])
            if url_list[k][3] is None:
                url_score.append(-1.)
            else:
                url_score.append(url_list[k][3])

        result_strings.append([url_scheme,url_domain,url_port,url_score])

    if report_class.strings.domains_randomness_score is None:
        result_strings.append([-1,-1,-1,-1,-1])
    else:
        scores = list(report_class.strings.domains_randomness_score)
        scores_new = [x for x in scores if x is not None]
        if len(scores_new) == 0:
            randomness_score_stat = [-1,-1,-1,-1,-1]
        else:
            randomness_score_stat = [np.min(scores_new),np.max(scores_new),np.median(scores_new),np.mean(scores_new),np.std(scores_new)]

        result_strings.append(randomness_score_stat)
    #if report_class.strings is None:
    #   return ['UrlDomain: None']
    #else:
    #   urldomain = np.unique(list(report_class.strings))
    #   result_strings = []
    #   for k in range(len(urldomain)):
    #       result_strings.append('UrlDomain: ' + urldomain[k])

    #   tot_domains = report_class.strings.tot_domains
    #   domain_rand_score = report_class.strings.domains_randomness_score

    return result_strings # [tot_domains, tot_ips. tot_urls, url_scheme, url_domain, url_port, url_score,randomness score]

def NetworkExtraction(report_class):
    network_domain = []
    network_url_scheme = []
    network_url_domain = []
    network_url_port = []
    network_url_score = []
    network_lenudp = []
    network_udp_ports = []
    network_lentcp = []
    network_tcp_ports = []
    network_lendns = []
    network_useragent = []
    if len(report_class.network.domains) == 0:
        network_domain.append('No domain')
    else:
        domains = list(report_class.network.domains)
        network_domain.extend(domains)

    url_list = list(report_class.network.urls)
    for k in range(len(url_list)):
        network_url_scheme.append(url_list[k][0])
        if url_list[k][1] is None:
            network_url_domain.append(-1.)
        else:
            network_url_domain.append(url_list[k][1])

        network_url_port.append(url_list[k][2])
        if url_list[k][3] is None:
            network_url_score.append(-1.)
        else:
            network_url_score.append(url_list[k][3])

    network_lenudp.append(report_class.network.len_udp)
    if len(report_class.network.udp_ports) == 0:
        network_udp_ports.append(-1)
    else:
        network_udp_ports.extend(list(report_class.network.udp_ports))

    network_lentcp.append(report_class.network.len_tcp)
    if len(report_class.network.tcp_ports) == 0:
        network_tcp_ports.append(-1)
    else:
        network_tcp_ports.append(list(report_class.network.tcp_ports))

    network_lendns.append(report_class.network.len_dns)
    if len(report_class.network.http_useragents) == 0:
        network_useragent.append('No useragent')
    else:
        network_useragent.append(list(report_class.network.http_useragents))

    return [report_class.network.len_udp,report_class.network.len_tcp,report_class.network.len_dns] 

def ProcessExtraction(report_class):
    len_injected = report_class.processes.len_injected
    len_terminated = report_class.processes.len_terminated
    len_created = report_class.processes.len_created
    len_shellcmds = report_class.processes.len_shellcmds

    injected_string = []
    created_string = []
    shellcmds_string = []
    terminated_string = []


    result_strings = []

    if report_class.processes.injected_exe is None:
        #injected_string.append('Process Injected: None')
        injected_string.append('Process Injected: None')
    else:
        injected_string.extend(list(report_class.processes.injected_exe))

    if report_class.processes.created is None:
        #created_string.append('Process Created: None')
        created_string.append('Process Created: None')
    else:
        created_string = list(report_class.processes.created)

    if report_class.processes.shellcmds is None:
        #shellcmds_string.append('Process ShellCMD: None')
        shellcmds_string.append('Process ShellCMD: None')
    else:
        shellcmds_string = list(report_class.processes.shellcmds)

    if report_class.processes.terminated_exe is None:
        #terminated_string.append('Process Terminated: None')
        terminated_string.append('Process Terminated: None')
    else:
        terminated_string = list(report_class.processes.terminated_exe)
        
    return [len_injected,len_created,len_shellcmds,len_terminated]

def ServiceExtraction(report_class):
    service_string = []
    if report_class.services is None:
        service_string.append('Service: None')
    else:
        service_string = list(report_class.services)

    #randomness_score = []
    if report_class.services_names_randomness is None:
        randomness_score_stat = [-1,-1,-1,-1,-1]
    else:
        scores = list(report_class.services_names_randomness)
        randomness_score_stat = [np.min(scores),np.max(scores),np.median(scores),np.mean(scores),np.std(scores)]
        

    return [service_string,randomness_score_stat,report_class.tot_services] 


def RegistryExtraction(report_class):
    deleted_string = []
    set_string = []

    if report_class.registry.deleted is None:
        deleted_string.append('Deleted: None')
    else:
        deleted_string = list(report_class.registry.deleted)

    if report_class.registry.set is None:
        set_string.append('Set: None')
    else:
        set_string = list(report_class.registry.set)

    return [deleted_string,set_string]


def MutexExtraction(report_class):
    mutex_string = []
    if report_class.mutexes is None:
        mutex_string.append('Mutex: None')
    else:
        mutex_string = list(report_class.mutexes)

    #randomness_score_stat = []
    if report_class.mutexes_names_randomness is None:
        randomness_score_stat = [-1,-1,-1,-1,-1]
    else:
        scores = list(report_class.mutexes_names_randomness)
        scores_new = [x for x in scores if x is not None]
        randomness_score_stat = [np.min(scores_new),np.max(scores_new),np.median(scores_new),np.mean(scores_new),np.std(scores_new)]


    return [mutex_string,randomness_score_stat,report_class.tot_mutexes]



def RunTimeDllExtraction(report_class):
    #dll_default_string = []
    #dll_system_string = []
    #dll_custom_string = []
    len_default = report_class.runtime_dlls.len_default
    len_system = report_class.runtime_dlls.len_system
    len_custom = report_class.runtime_dlls.len_custom
    if len_default + len_system + len_custom < 1.:
        random_score_stat = [-1,-1,-1,-1,-1]
        return [len_default,len_system,len_custom]
    else:
        scores = list(report_class.runtime_dlls.dll_name_randomness_scores)
        #random_score_stat.append(np.min(scores),np.max(scores),np.median(scores),np.mean(scores),np.std(scores))


    return [len_default,len_system,len_custom] 

def FileFeatureEncode(file_feature):
    #[read_dir_name,read_rand_stat,len_read,written_dir_name,written_rand_stat,len_written,deleted_dir_name,deleted_rand_stat,len_deleted]
    ##### for reading operations
    dir_read_strings = []
    for k in range(len(file_feature)):
        dir_read_strings.extend(file_feature[k][0])

    dir_read_strings = list(set(dir_read_strings)) #### remove the repetitive strings 

    dir_written_strings = []
    for k in range(len(file_feature)):
        dir_written_strings.extend(file_feature[k][3])

    dir_written_strings = list(set(dir_written_strings)) #### remove the repetitive strings 

    dir_deleted_strings = []
    for k in range(len(file_feature)):
        dir_deleted_strings.extend(file_feature[k][6])

    dir_deleted_strings = list(set(dir_deleted_strings)) #### remove the repetitive strings 

    file_feature_code = []
    for k in range(len(file_feature)):
        read_onehot_code = np.zeros((len(dir_read_strings),))
        read_strings = file_feature[k][0]
        for i in range(len(read_strings)):
            idx = np.where(np.array(dir_read_strings) == read_strings[i])[0]
            read_onehot_code[idx] += 1. 

        read_code = read_onehot_code.tolist() + file_feature[k][1] + [file_feature[k][2]]

        written_onehot_code = np.zeros((len(dir_written_strings),))
        written_strings = file_feature[k][3]
        for i in range(len(written_strings)):
            idx = np.where(np.array(dir_written_strings) == written_strings[i])[0]
            written_onehot_code[idx] += 1. 

        written_code = written_onehot_code.tolist() + file_feature[k][4] + [file_feature[k][5]]

        deleted_onehot_code = np.zeros((len(dir_deleted_strings),))
        deleted_strings = file_feature[k][6]
        for i in range(len(deleted_strings)):
            idx = np.where(np.array(dir_deleted_strings) == deleted_strings[i])[0]
            deleted_onehot_code[idx] += 1. 

        deleted_code = deleted_onehot_code.tolist() + file_feature[k][7] + [file_feature[k][8]]

        file_feature_code.append(read_code + written_code + deleted_code)

    return file_feature_code 
 
def MutexFeatureEncode(mutex_feature):
    #[mutex_string,randomness_score_stat,report_class.tot_mutexes]
    mutex_strings = []
    for k in range(len(mutex_feature)):
        mutex_strings.extend(mutex_feature[k][0])

    mutex_strings = list(set(mutex_strings))
    nlen = len(mutex_strings)
    mutex_feature_code = []
    for k in range(len(mutex_feature)):
        mutex_onehot_code = np.zeros((nlen,))
        one_mutex_strings = mutex_feature[k][0]
        for i in range(len(one_mutex_strings)):
            idx = np.where(np.array(mutex_strings) == one_mutex_strings[i])[0]
            mutex_onehot_code[idx] += 1. 

        mutex_code = mutex_onehot_code.tolist() + mutex_feature[k][1] + [mutex_feature[k][2]]
        mutex_feature_code.append(mutex_code)

    return mutex_feature_code

def RegistryFeatureEncode(registry_feature):
    #[deleted_string,set_string]
    unique_deleted_string = []
    unique_set_string = []
    for k in range(len(registry_feature)):
        unique_deleted_string.extend(registry_feature[k][0])
        unique_set_string.extend(registry_feature[k][1])

    unique_deleted_string = list(set(unique_deleted_string))
    unique_set_string = list(set(unique_set_string))

    registry_feature_code = []
    nlen_deleted = len(unique_deleted_string)
    nlen_set = len(unique_set_string)

    for k in range(len(registry_feature)):
        deleted_onehot_code = np.zeros((nlen_deleted,))
        one_deleted_strings = registry_feature[k][0]
        for i in range(len(one_deleted_strings)):
            idx = np.where(np.array(unique_deleted_string) == one_deleted_strings[i])[0]
            deleted_onehot_code[idx] += 1. 

        set_onehot_code = np.zeros((nlen_set,))
        one_set_strings = registry_feature[k][1]
        for i in range(len(one_set_strings)):
            idx = np.where(np.array(unique_set_string) == one_set_strings[i])[0]
            set_onehot_code[idx] += 1. 

        registry_code = deleted_onehot_code.tolist() + set_onehot_code.tolist()
        registry_feature_code.append(registry_code)

    return registry_feature_code

def ServiceFeatureEncode(service_feature):
    #[service_string,randomness_score_stat,report_class.tot_services] 
    unique_service_string = []
    for k in range(len(service_feature)):
        unique_service_string.extend(service_feature[k][0])

    unique_service_string = list(set(unique_service_string))
    nlen = len(unique_service_string)
    service_feature_code = []

    for k in range(len(service_feature)):
        service_onehot_code = np.zeros((nlen,))
        one_service_strings = service_feature[k][0]
        for i in range(len(one_service_strings)):
            idx = np.where(np.array(unique_service_string) == one_service_strings[i])[0]
            service_onehot_code[idx] += 1. 

        service_code = service_onehot_code.tolist() + service_feature[k][1] + [service_feature[k][2]]
        service_feature_code.append(service_code)

    return service_feature_code 

def UrldomainFeatureEncode(string_feature):
    #[tot_domains, tot_ips. tot_urls, [url_scheme, url_domain, url_port, url_score],randomness score]
    url_scheme_string = []
    url_port_string = []
    for k in range(len(string_feature)):
        url_scheme_string.extend(string_feature[k][3][0])
        url_port_string.extend(string_feature[k][3][2])

    url_scheme_string = list(set(url_scheme_string))
    url_port_string = list(set(url_port_string))
    nlen_scheme = len(url_scheme_string)
    nlen_port = len(url_port_string)

    url_feature_code = []
    for k in range(len(string_feature)):
        scheme_onehot_code = np.zeros((nlen_scheme,))
        port_onehot_code = np.zeros((nlen_port,))
        one_scheme_string = string_feature[k][3][0]
        one_port_string = string_feature[k][3][2]
        for i in range(len(one_scheme_string)):
            idx = np.where(np.array(url_scheme_string) == one_scheme_string[i])[0]
            scheme_onehot_code[idx] += 1.

        for i in range(len(one_port_string)):
            idx = np.where(np.array(url_port_string) == one_port_string[i])[0]
            port_onehot_code[idx] += 1.

        min_score = np.min(string_feature[k][3][1])
        max_score = np.max(string_feature[k][3][1])
        mean_score = np.mean(string_feature[k][3][1])
        median_score = np.median(string_feature[k][3][1])
        std_score = np.std(string_feature[k][3][1])



        url_code = [string_feature[k][0]] + [string_feature[k][1]] + [string_feature[k][2]] + scheme_onehot_code.tolist() + [min_score,max_score,mean_score,median_score,std_score] + port_onehot_code.tolist() + string_feature[k][4] 
        url_feature_code.append(url_code)

    return url_feature_code

def ResourceFeatureEncode(resource_feature):
    #[min_score,max_score,mean_score,median_score,std_score,resource_strings]
    unique_resource_string = []
    for k in range(len(resource_feature)):
        unique_resource_string.extend(resource_feature[k][-1])

    unique_resource_string = list(set(unique_resource_string))
    nlen_resource = len(unique_resource_string)

    resource_feature_code = []
    for k in range(len(resource_feature)):
        resource_onehot_code = np.zeros((nlen_resource,))
        one_resource_string = resource_feature[k][-1]
        for i in range(len(one_resource_string)):
            idx = np.where(np.array(unique_resource_string) == one_resource_string[i])[0]
            resource_onehot_code[idx] += 1.


        resource_code = [resource_feature[k][0]] + [resource_feature[k][1]] + [resource_feature[k][2]] + [resource_feature[k][3]] + [resource_feature[k][4]] + resource_onehot_code.tolist()
        resource_feature_code.append(resource_code)

    return resource_feature_code

def ImportFeatureEncode(import_feature):
    unique_import_string = []
    for k in range(len(import_feature)):
        unique_import_string.extend(import_feature[k][-1])

    unique_import_string = list(set(unique_import_string))
    nlen_import = len(unique_import_string)

    import_feature_code = []
    for k in range(len(import_feature)):
        import_onehot_code = np.zeros((nlen_import,))
        one_import_string = import_feature[k]
        for i in range(len(one_import_string)):
            idx = np.where(np.array(unique_import_string) == one_import_string[i])[0]
            import_onehot_code[idx] += 1.

        import_feature_code.append(import_onehot_code.tolist())

    return import_feature_code 


def SectionFeatureEncode(section_feature):
    unique_section_string = []
    for k in range(len(section_feature)):
        unique_section_string.extend(section_feature[k][-1])

    unique_section_string = list(set(unique_section_string))
    nlen = len(unique_section_string)

    section_feature_code = []
    for k in range(len(section_feature)):
        section_onehot_code = np.zeros((nlen,))
        one_section_string = section_feature[k][-1]
        for i in range(len(one_section_string)):
            idx = np.where(np.array(unique_section_string) == one_section_string[i])[0]
            section_onehot_code[idx] += 1.

        section_code = section_feature[k][0:10] + section_onehot_code.tolist()
        section_feature_code.append(section_code)


    return section_feature_code 


def HeaderMetaFeatureEncode(hm_feature):
    #[ep, epsec, sub, ts_int, ts_year, hm_size, hm_od]
    unique_ep_string = []
    for k in range(len(hm_feature)):
        unique_ep_string.append(hm_feature[k][0])

    unique_ep_string = list(set(unique_ep_string))
    nlen_ep = len(unique_ep_string)

    unique_epsec_string = []
    for k in range(len(hm_feature)):
        unique_epsec_string.append(hm_feature[k][1])

    unique_epsec_string = list(set(unique_epsec_string))
    nlen_epsec = len(unique_epsec_string)

    unique_sub_string = []
    for k in range(len(hm_feature)):
        unique_sub_string.extend(hm_feature[k][2])

    unique_sub_string = list(set(unique_sub_string))
    nlen_sub = len(unique_sub_string)

    hm_feature_code = []
    for k in range(len(hm_feature)):
        ep_onehot_code = np.zeros((nlen_ep,))
        epsec_onehot_code = np.zeros((nlen_epsec,))
        sub_onehot_code = np.zeros((nlen_sub,))
        one_ep_string = hm_feature[k][0]
        one_epsec_string = hm_feature[k][1]
        one_sub_string = hm_feature[k][2]

        if isinstance(one_ep_string,list):
            for i in range(len(one_ep_string)):
                idx = np.where(np.array(unique_ep_string) == one_ep_string[i])[0]
                ep_onehot_code[idx] += 1

        else:
            idx = np.where(np.array(unique_ep_string) == one_ep_string)[0]
            ep_onehot_code[idx] += 1

        if isinstance(one_epsec_string,list):
            for i in range(len(one_epsec_string)):
                idx = np.where(np.array(unique_epsec_string) == one_epsec_string[i])[0]
                epsec_onehot_code[idx] += 1

        else:
            idx = np.where(np.array(unique_epsec_string) == one_epsec_string)[0]
            epsec_onehot_code[idx] +=1


        if isinstance(one_sub_string,list):
            for i in range(len(one_sub_string)):
                idx = np.where(np.array(unique_sub_string) == one_sub_string[i])[0]
                sub_onehot_code[idx] += 1

        else:
            idx = np.where(np.array(unique_sub_string) == one_sub_string)[0]
            sub_onehot_code[idx] += 1


        hm_code = ep_onehot_code.tolist() + epsec_onehot_code.tolist() + sub_onehot_code.tolist() + hm_feature[k][3:-1] + [int(hm_feature[k][-1])]
        hm_feature_code.append(hm_code)

    return hm_feature_code

def SignerFeatureEncode(signer_feature):

    unique_singer_string = []
    for k in range(len(signer_feature)):
        unique_singer_string.append(signer_feature[k])

    unique_signer_string = list(set(unique_singer_string))
    nlen = len(unique_singer_string)

    signer_feature_code = []
    for k in range(len(signer_feature)):
        signer_onehot_code = np.zeros((nlen,))
        one_signer_string = signer_feature[k]

        if isinstance(one_signer_string,list):
            for i in range(len(one_signer_string)):
                idx = np.where(np.array(unique_signer_string) == one_signer_string[i])[0]
                signer_onehot_code[idx] += 1

        else:
            idx = np.where(np.array(unique_signer_string) == one_signer_string)[0]
            signer_onehot_code[idx] += 1

        signer_feature_code.append(signer_onehot_code)

    return signer_feature_code





def load_dataset() -> Tuple[List[Report], List[Report], List[Report]]:
    with Pool() as pool:
        start_time = time.time()

        malware = pool.map(load_report, os_listdir_json(malware_folder))
        for m in malware:
            assert m.is_malware

        goodware = pool.map(load_report, os_listdir_json(goodware_folder))
        for g in goodware:
            assert not g.is_malware

        len_goodware = len(goodware)
        len_malware = len(malware)
        print(len_goodware)
        print(len_malware)
        #assert len_goodware == len_malware

        #game = pool.map(load_report, os_listdir_json(game_folder))
        #assert len(game) == 20
        #gm = gg = i = 0
        #for g in game:
        #    i += 1
        #    print(i, g.sha256)
        #    if g.is_malware:
        #        gm += 1
        #    else:
        #        gg += 1
        #assert gm == 11 and gg == 9
        #print(f'> Dataset loaded in {round(time.time() - start_time, 1)}sec. #malware={len_malware} #goodware={len_goodware}')
    #print()
    return malware, goodware

malware,goodware = load_dataset()

file_feature = []
dll_feature = []
mutex_feature = []
registry_feature = []
service_feature = []
process_feature = []
urldomain_feature = []
resource_feature = []
imports_feature = []
sections_feature = []
HeaderMeta_feature = []
signer_feature = []
network_feature = []


#len_goodware = len(goodware) 

all_train_data = goodware + malware

for i in range(len(all_train_data)):
    instance_sample = all_train_data[i]
    file_feature.append(FileSystemExtraction(instance_sample))
    dll_feature.append(RunTimeDllExtraction(instance_sample))
    mutex_feature.append(MutexExtraction(instance_sample))
    registry_feature.append(RegistryExtraction(instance_sample))
    service_feature.append(ServiceExtraction(instance_sample))
    process_feature.append(ProcessExtraction(instance_sample))
    urldomain_feature.append(StringsExtraction(instance_sample))
    resource_feature.append(ResourcesExtraction(instance_sample))
    imports_feature.append(ImportsExtraction(instance_sample))
    sections_feature.append(SectionExtraction(instance_sample))
    HeaderMeta_feature.append(HeaderMetaExtraction(instance_sample))
    signer_feature.append(SignatureExtraction(instance_sample))
    network_feature.append(NetworkExtraction(instance_sample))


file_feature_mat = FileFeatureEncode(file_feature)
#### dll_feature is 3-element list, each one is a numerical value 
mutex_feature_mat = MutexFeatureEncode(mutex_feature)
registry_feature_mat = RegistryFeatureEncode(registry_feature)
service_feature_mat = ServiceFeatureEncode(service_feature)
#### process feature is 4-element list, each one is a numerical value
url_feature_mat = UrldomainFeatureEncode(urldomain_feature)
resource_feature_mat = ResourceFeatureEncode(resource_feature)
import_feature_mat = ImportFeatureEncode(imports_feature)
section_feature_mat = SectionFeatureEncode(sections_feature)
hm_feature_mat = HeaderMetaFeatureEncode(HeaderMeta_feature)
signer_feature_mat = SignerFeatureEncode(signer_feature)
#### network feature is 3-element list, each one is a numerical value 
feature_mat = np.concatenate((np.array(file_feature_mat),np.array(dll_feature),np.array(mutex_feature_mat),np.array(registry_feature_mat),np.array(service_feature_mat),np.array(process_feature),
    np.array(url_feature_mat),np.array(resource_feature_mat),np.array(import_feature_mat),np.array(section_feature_mat),np.array(hm_feature_mat),np.array(signer_feature_mat),np.array(network_feature)),axis=1)



train_label = (-1)*np.ones((len(all_train_data),))
train_label[0:len(goodware)] = 1
train_label = np.array(train_label)
###### cross-validation 

auc_score = []
pos_idx = np.array(range(0,len(goodware)))
neg_idx = np.array(range(len(goodware),len(train_label)))
clf = RandomForestClassifier(n_estimators = 500)
for iround in range(5):
    np.random.shuffle(pos_idx)
    np.random.shuffle(neg_idx)
    train_idx = pos_idx[:int(float(len(goodware)) * 0.8)].tolist() + neg_idx[:int(float(len(malware))*0.8)].tolist()
    test_idx = pos_idx[int(float(len(goodware)) * 0.8):].tolist() + neg_idx[int(float(len(malware))*0.8):].tolist()
    clf.fit(feature_mat[train_idx,:],train_label[train_idx])
    decision_score = clf.predict_proba(feature_mat[test_idx,:])[:,1]
    auc_score.append(roc_auc_score(train_label[test_idx],decision_score))
    print(auc_score)
#def main():
#    malware, goodware, game = load_dataset()
    #sys.exit()
#    all = list()
#    for reports in [malware, goodware]:
#        for r in reports:
#            r: Report
#            all.extend(r.network.http_useragents)
#            '''
#            if r.registry.set is not None:
#                print(r.registry.set)
#            if r.registry.deleted is not None:
#                print(r.registry.deleted)
            
#            if r.header_meta.timestamp is not None:
#                print(r.header_meta.timestamp)
#                all.add(r.header_meta.timestamp)
#            '''
        
#    from collections import Counter
#    print(Counter(all))
#    print((len(all)))


#if __name__ == "__main__":
#    main()

