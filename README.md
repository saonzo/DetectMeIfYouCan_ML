## DetectMeIfYouCan_ML

The dataset is [here](https://www.dropbox.com/s/57w2y1ghrvxdvmt/dataset.7z?dl=1)!

MD5(dataset.7z) = ``3db68a07427499c2396b3f1be1b444e5``

How to run from command line:
```bash
/DetectMeIfYouCan_ML$ python3 -m src.main
```

## Feature Vectorization

We present more details about how we performed feature vectorization.
First, we must introduce how and why, we used a \emph{Gibberish Score} (GScore, in short) -- gibberish means spoken or written words that have no meaning.
Sandbox reports are rich with strings, like process names, file paths, and URLs, and legitimate ones are often meaningful names because such strings come from good programming practices where a name should make people understand why it exists, what it does, and how it is used.
Hence, in addition to well-known practices for feature vectorization discussed above, we also involve our GScore that works as follows.
We built a Markov Chain of character to character transition probabilities from the words in the English dictionary, files and folder names of a clean Windows installation, and the top 500 domain names of the Alexa ranking.
To calculate the GScore of a given string, we walk through the Markov Chain and compute the product of the transition probabilities.
Hence, our GScore is a function that takes a string and returns a real number between 0 and 1 (not included), i.e., $GScore: String \rightarrow (0, 1)$.
A high - close to 1 - GScore means that it is very likely that it is a meaningful string, while a lower - close to 0 - GScore indicates a high chance of a gibberish string.

Moreover, we make another little digression to explain how we managed the file paths, i.e., a file system location.
When dealing with disk accesses, the sample under analysis transfers data between the main and a target file in the persistent memory.
Even starting a process or dynamically loading a DLL involves disk access to retrieve the executable or the DLL.
Since we encounter file paths very frequently, we have generalized the way we handle them.
Given a string containing the file path, we consider the: parent folder, extension, and GScore of the file name.
The parent folder suggests if the file resides in a reliable folder, like the system ones, while the extension on the Windows operating system defines the file type (e.g., `.exe` for executables).
We consider the parent folder and the extension as categorical, while the GScore as numerical.


We discuss below the techniques we used to convert static and dynamic features into numerical feature vectors.
Raw static and dynamic analysis reports are encoded into computable feature vectors to train Machine Learning based models in the malware classification task. 
Most of the raw attributes in the reports are categorical variables without an ordinal relationship. 
We follow a one-hot encoding scheme for each of these attributes: we first count all the unique categories that one such attribute can carry. 
Then each attribute is transformed into a bit vector, with each bit representing a possible category. 
One bit in the vector is set to 1, only if the attribute takes the corresponding category value. 
Otherwise, it is set to 0. 
Before illustrating the remaining details about the feature encoding scheme for each feature class below, we point out that we refer to `summary statistics` to indicate min, max, average, median, and standard deviation of a series of numbers.

### Signature
We treat the signers (e.g., Microsoft, Dell, Google, etc.) as a categorical attribute, and we also dedicate one bit to indicate if the signature has been verified or not.

### Header Metadata
First, we consider the section's name containing the entry point and the sub-system (if it is a command line or a GUI executable, and the target architecture) as categorical features. The timestamp, the size of the file, and the size of possible overlay data are used as numerical features. 

### Sections
We compute the summary statistics of the size and entropy scores for each section, considering them as numerical features.
Instead, we consider the names of the sections as a categorical feature.

### Imports
For each entry, we merge the DLL's name with the function's name, and we consider this new string as a categorical feature.

### Resources
We compute the summary statistics of the entropy scores as numerical features. Besides, we consider the string that indicates the type of resource as categorical.

### Strings
We filter all the string, using regular expressions, and we keep: URLs, registry keys, file paths, and domain names. Then, each remaining string is treated as a categorical attribute, while we calculate the summary statistics of the GScores of the extracted strings.

### Network
We count the number of UDP, TCP, and DNS connections as numerical features separately while considering TCP and UDP ports as categorical.
For each domain involved in the DNS protocol, we consider the top-level domain as categorical, while we compute the summary statistics of the GScores on the remaining domain name.

### Processes
We count the number of the injected, created, terminated, and shell commands as separated numerical features.
Moreover, if we encounter a file path, we manage it as a categorical attribute as described before.

### Services
We count the number of created and stopped services; for each service, we consider the summary statistics of the GScores of the service names and each of  the names as categorical.

### Registry
Actions on the Windows registry can be summarized in: `set` and `delete`. Each action operates on a certain key; hence, we treat the keys involved in set and delete action separately, considering them as categorical.

### Mutexes
We consider the count the number of mutexes created or opened (opening a mutex is often used to check if it exists), and the summary statistics of the GScores calculated on mutex names as numerical, while mutexes' name as categorical.

### File System
here are three types -- written, read, and deleted -- of operations on a file path; thus, we consider them separately.
For example, for every written file, we have the parent folders, the extensions, and the summary statistics of the GScores of the file names.

### Runtime DLLs
Processes can load dynamically DLLs, and they can also control the location from which a DLL is loaded by specifying a full path.
Therefore, we count separately how many DLLs are loaded: without specifying a path, a path within the Windows system folder, a path outside system folders.
Then, every file path is treated as a categorical attribute in our standard way.
