import numpy as np 
import datetime 
import time

from os.path import isdir, join, abspath, dirname
from pathlib import Path
from typing import List, Optional, Tuple

from src.Report import Report
from src.utility import os_listdir_json, read_json
from src.dataset_checks import dataset_checks

from sklearn.ensemble import RandomForestClassifier
import pickle

#class Report:
#    is_malware: bool
#    signature: Optional[Signature]
#    header_meta: HeaderMeta
#    sections: Optional[List[Section]]
#    imports: Optional[Set[str]]
#    resources: Optional[List[Resource]]
#    strings: Optional[Set[str]]
#    processes: Processes
#    services: Optional[Set[str]]
#    registry: Optional[Set[str]]
#    mutexes: Optional[Set[str]]
#    file_system: FileSystem
#    runtime_dlls: RuntimeDLLs
def feature_extract_strings(strfeature,unique_str):
	numfeature = np.zeros((len(unique_str),))
	for i in range(len(unique_str)):
		if unique_str[i] in strfeature:
			numfeature[i] = 1.

	return numfeature 


def unique_name_section(section_strings):
	names = list()
	for i in range(len(section_strings)):
		names.extend(section_strings[i][-1])

	return np.unique(np.array(names))

def unique_subsystem(header_meta_strings):
	subsys = list()
	for i in range(len(header_meta_strings)):
		subsys.append(header_meta_strings[i][1])

	return np.unique(np.array(subsys))

def flatten_dict(resource_dict):
	resource_types = []
	for i in range(len(resource_dict)):
		resource_types.extend(list(resource_dict[i].keys()))

	return np.unique(np.array(resource_types))


def flatten_list(string_list):
	flat_strings = []
	for sublist in string_list:
		for single_string in sublist:
			flat_strings.append(single_string)

	return np.unique(np.array(flat_strings))


def get_reports(tgt_folder: str) -> List[Report]:
    return [Report(read_json(j)) for j in os_listdir_json(tgt_folder)]

def FilePathExtraction(report_class):

	if report_class.file_system.read is None:
		read_string = []
	else:
		read_string = list(report_class.file_system.read)

	if report_class.file_system.written is None:
		written_string = []
	else:
		written_string = list(report_class.file_system.written)

	if report_class.file_system.deleted is None:
		deleted_string = []
	else:
		deleted_string = list(report_class.file_system.deleted)
	
	read_path = []
	written_path = []
	deleted_path = []

	if len(read_string) == 0:
		read_path.append('read: None')

	if len(written_string) == 0:
		written_path.append('written: None')

	if len(deleted_string) == 0:
		deleted_path.append('deleted: None')

	if len(read_string) > 0:
		for k in range(len(read_string)):
			path_segments = read_string[k].split('\\')
			last_component = path_segments[-1]
			if last_component.find('.') <0: 
				read_path.append('read: ' + read_string[k])
			else:
				##### remove the last component
				tmp_string = 'read: '
				for i in range(len(path_segments)-1):
					if i == len(path_segments)-2:
						tmp_string += path_segments[i]
					else:
						tmp_string += path_segments[i] + '\\'

				read_path.append(tmp_string)

	if len(written_string) > 0:
		for k in range(len(written_string)):
			path_segments = written_string[k].split('\\')
			last_component = path_segments[-1]
			if last_component.find('.') <0: 
				written_path.append('written: ' + written_string[k])
			else: 
				##### remove the last component
				tmp_string = 'written: '
				for i in range(len(path_segments)-1):
					if i == len(path_segments)-2:
						tmp_string += path_segments[i]
					else:
						tmp_string += path_segments[i] + '\\' 

				written_path.append(tmp_string)

	if len(deleted_string) > 0:
		for k in range(len(deleted_string)):
			path_segments = deleted_string[k].split('\\')
			last_component = path_segments[-1]
			if last_component.find('.') <0: 
				deleted_path.append('deleted: ' + deleted_string[k])
			else:
				##### remove the last component
				tmp_string = 'deleted: '
				for i in range(len(path_segments)-1):
					if i == len(path_segments)-2:
						tmp_string += path_segments[i]
					else:
						tmp_string += path_segments[i] + '\\' 

				deleted_path.append(tmp_string)


	return read_path + written_path + deleted_path


def SignatureExtraction(report_class):
	signer = ''
	if report_class.signature is None:
		signer = 'Signer: None'
	else:
		signer += str(report_class.signature.signers) 
		if report_class.signature.verified is True:
			signer += '; ' + 'True'
		else:
			signer += '; ' + 'False'
		#signer += '; ' + report_class.signature.verified

	return signer 


def HeaderMetaExtraction(report_class):
	hm = report_class.header_meta
	if hm.entrypoint is None:
		ep = -1
	else:
		ep = hm.entrypoint
	
	if hm.subsystem is None:
		sub = 'HeaderMeta Subsystem: None'
	else:
		sub = hm.subsystem
	
	if hm.timestamp is None:
		ts_int = -1
	else:
		ts_string_segs = hm.timestamp.split(' ')
		#print(hm.timestamp)
		part1 = ts_string_segs[0].split(':')
		part2 = ts_string_segs[1].split('+')
		#part3 = part2[0].split(':')
		#part4 = part2[1].split(':')
		year = part1[0]
		month = part1[1]
		date = part1[2]
		if int(year) == 0:
			ts_int = 0
		else:
			time_string = year + '-' + month + '-' + date + 'T' + ts_string_segs[1]
			#print(time_string)
			#hour_int = int(hour)
			ts_stamp = datetime.datetime.fromisoformat(time_string)
			ts_int = ts_stamp.timestamp()


	return [ep,sub,ts_int]


def SectionExtraction(report_class):
	sections = report_class.sections
	vsize = []
	entropy = []
	name = []

	if sections is None:
		vsize = -1
		entropy = -1
		name.append('sections: None')
		return [vsize,vsize,vsize,entropy,entropy,entropy,name]

	for k in range(len(sections)):
		vsize.append(sections[k].vsize)
		entropy.append(sections[k].entropy)
		name.append(sections[k].name)

	return [np.min(vsize),np.max(vsize),np.median(vsize),np.min(entropy),np.max(entropy),np.median(entropy),np.unique(np.array(name)).tolist()]

def ImportsExtraction(report_class):
	if report_class.imports is None:
		import_strings = []
		import_strings.append('Import: None')
	else:
		import_strings = list(report_class.imports)
	
	return import_strings 

def ResourcesExtraction(report_class):
	if report_class.resources is None:
		returned_resource_dict = dict()
		returned_resource_dict['Resource: None'] = -1
	else:
		resources = report_class.resources
		resource_dict = dict()
		returned_resource_dict = dict()
		for k in range(len(resources)):
			resource_dict[resources[k].type] = []
			returned_resource_dict[resources[k].type] = 0.

		for k in range(len(resources)):
			if resources[k].entropy is None:
				resource_dict[resources[k].type].append(0.)
			else:
				resource_dict[resources[k].type].append(resources[k].entropy)
			#resource_dict[resources[k].type].append(resources[k].entropy)

		print(list(resource_dict.keys()))

		for single_key in list(resource_dict.keys()):
			print(resource_dict[single_key])
			if np.median(resource_dict[single_key]) == 0.:
				returned_resource_dict[single_key] = 0.
			else:
				idx = np.where(np.array(resource_dict[single_key]) > 0)[0]
				returned_resource_dict[single_key] = np.median(np.array(resource_dict[single_key])[idx])


	return returned_resource_dict 

def StringsExtraction(report_class):
	if report_class.strings is None:
		return ['UrlDomain: None']
	else:
		urldomain = np.unique(list(report_class.strings))
		result_strings = []
		for k in range(len(urldomain)):
			result_strings.append('UrlDomain: ' + urldomain[k])

		return result_strings 

def ProcessExtraction(report_class):
	injected_string = []
	created_string = []
	shellcmds_string = []
	terminated_string = []
	result_strings = []

	if report_class.processes.injected is None:
		#injected_string.append('Process Injected: None')
		result_strings.append('Process Injected: None')
	else:
		injected_string = list(report_class.processes.injected)
		for i in range(len(injected_string)):
			result_strings.append('Process Injected: ' + injected_string[i])

	if report_class.processes.created is None:
		#created_string.append('Process Created: None')
		result_strings.append('Process Created: None')
	else:
		created_string = list(report_class.processes.created)
		for i in range(len(created_string)):
			result_strings.append('Process Created: ' + created_string[i])

	if report_class.processes.shellcmds is None:
		#shellcmds_string.append('Process ShellCMD: None')
		result_strings.append('Process ShellCMD: None')
	else:
		shellcmds_string = list(report_class.processes.shellcmds)
		for i in range(len(shellcmds_string)):
			result_strings.append('Process ShellCMD: ' + shellcmds_string[i])

	if report_class.processes.terminated is None:
		#terminated_string.append('Process Terminated: None')
		result_strings.append('Process Terminated: None')
	else:
		terminated_string = list(report_class.processes.terminated)
		for i in range(len(terminated_string)):
			result_strings.append('Process Terminated: ' + terminated_string[i])

	return result_strings

def ServiceExtraction(report_class):
	service_string = []
	if report_class.services is None:
		service_string.append('Service: None')
	else:
		service_string = list(report_class.services)

	return service_string 


def RegistryExtraction(report_class):
	registry_string = []
	if report_class.registry is None:
		registry_string.append('Registry: None')
	else:
		registry_string = list(report_class.registry)

	return registry_string


def MutexExtraction(report_class):
	mutex_string = []
	if report_class.mutexes is None:
		mutex_string.append('Mutex: None')
	else:
		mutex_string = list(report_class.mutexes)

	return mutex_string

def RunTimeDllExtraction(report_class):
	#dll_default_string = []
	#dll_system_string = []
	#dll_custom_string = []
	result_strings = []

	if report_class.runtime_dlls.default is None:
		#dll_default_string = 'DLL Default: None'
		result_strings.append('DLL Default: None')
	else:
		dll_default_string = list(report_class.runtime_dlls.default)
		for k in range(len(dll_default_string)):
			result_strings.append('DLL Default: ' + dll_default_string[k])

	if report_class.runtime_dlls.system is None:
		#dll_system_string = 'DLL System: None'
		result_strings.append('DLL System: None')
	else:
		dll_system_string = list(report_class.runtime_dlls.system)
		for k in range(len(dll_system_string)):
			result_strings.append('DLL System: ' + dll_system_string[k])

	if report_class.runtime_dlls.custom is None:
		#dll_custom_string = 'DLL Custom: None'
		result_strings.append('DLL Custom: None')
	else:
		dll_custom_string = list(report_class.runtime_dlls.custom)
		for k in range(len(dll_custom_string)):
			result_strings.append('DLL System: ' + dll_custom_string[k])


	return result_strings 


train_feature = []
file_strings = []
dll_strings = []
mutex_strings = []
registry_strings = []
service_strings = []
process_strings = []
urldomain_strings = []
resource_strings_dict = []
imports_strings = []
sections_features = []
HeaderMeta_features = []
signer_strings = []

goodware_folder = 'dataset/goodware/'
malware_folder = 'dataset/malware/'


malware = get_reports(malware_folder)
len_malware = len(malware)
goodware = get_reports(goodware_folder)
len_goodware = len(goodware) 

all_train_data = goodware + malware

for i in range(len(all_train_data)):
	instance_sample = all_train_data[i]
	file_strings.append(FilePathExtraction(instance_sample))
	dll_strings.append(RunTimeDllExtraction(instance_sample))
	mutex_strings.append(MutexExtraction(instance_sample))
	registry_strings.append(RegistryExtraction(instance_sample))
	service_strings.append(ServiceExtraction(instance_sample))
	process_strings.append(ProcessExtraction(instance_sample))
	urldomain_strings.append(StringsExtraction(instance_sample))
	resource_strings_dict.append(ResourcesExtraction(instance_sample))
	imports_strings.append(ImportsExtraction(instance_sample))
	sections_features.append(SectionExtraction(instance_sample))
	HeaderMeta_features.append(HeaderMetaExtraction(instance_sample))
	signer_strings.append(SignatureExtraction(instance_sample))




unique_file_word = flatten_list(file_strings)
unique_dll_word = flatten_list(dll_strings)
unique_mutex_word = flatten_list(mutex_strings)
unique_registry_word = flatten_list(registry_strings)
unique_service_word = flatten_list(service_strings)
unique_process_word = flatten_list(process_strings)
unique_urldomain_word = flatten_list(urldomain_strings)
unique_import_word = flatten_list(imports_strings)
unique_signer_word = flatten_list(signer_strings)

unique_resource_types = flatten_dict(resource_strings_dict)
unique_subsys_hm_types = unique_subsystem(HeaderMeta_features)
unique_name_section_types = unique_name_section(sections_features)

num_features = []
for i in range(len(all_train_data)):
	num_file_feature = feature_extract_strings(file_strings[i],unique_file_word)
	num_dll_feature = feature_extract_strings(dll_strings[i],unique_dll_word)
	num_mutex_feature = feature_extract_strings(mutex_strings[i],unique_mutex_word)
	num_registry_feature = feature_extract_strings(registry_strings[i],unique_registry_word)
	num_service_feature = feature_extract_strings(service_strings[i],unique_registry_word)
	num_process_feature = feature_extract_strings(process_strings[i],unique_process_word)
	num_urldomain_feature = feature_extract_strings(urldomain_strings[i],unique_urldomain_word)
	num_import_feature = feature_extract_strings(imports_strings[i],unique_import_word)
	num_signer_feature = feature_extract_strings(signer_strings[i],unique_signer_word)
	num_features.append(np.concatenate((num_file_feature,num_dll_feature,num_mutex_feature,num_registry_feature,num_service_feature,num_process_feature,num_urldomain_feature,num_import_feature,num_signer_feature)))

#i = 0
#num_registry_feature = feature_extract_strings(registry_strings[i],unique_registry_word)
#num_service_feature = feature_extract_strings(service_strings[i],unique_service_word)
#num_process_feature = feature_extract_strings(process_strings[i],unique_process_word)
#num_urldomain_feature = feature_extract_strings(urldomain_strings[i],unique_urldomain_word)
#num_import_feature = feature_extract_strings(imports_strings[i],unique_import_word)
#num_signer_feature = feature_extract_strings(signer_strings[i],unique_signer_word)
#composed_fea = np.concatenate((num_file_feature,num_dll_feature,num_mutex_feature,num_registry_feature,num_service_feature,num_process_feature,num_urldomain_feature,num_import_feature,num_signer_feature))

resource_features = []
for i in range(len(all_train_data)):
	resource_per_instance = resource_strings_dict[i]
	resource_per_fea = np.zeros((len(unique_resource_types),))
	resource_per_instance_keys = list(resource_per_instance.keys())
	for j in range(len(resource_per_instance_keys)):
		idx = np.where(unique_resource_types == resource_per_instance_keys[j])[0]
		resource_per_fea[idx] = resource_per_instance[resource_per_instance_keys[j]]

	resource_features.append(resource_per_fea)


hm_features = []
for i in range(len(all_train_data)):
	hm_per_instance = HeaderMeta_features[i]
	hm_per_fea = np.zeros((len(unique_subsys_hm_types),))
	hm_per_instance_name = hm_per_instance[1]
	idx = np.where(unique_subsys_hm_types == hm_per_instance_name[j])[0]
	hm_per_fea[idx] = hm_per_instance[2]
	hm_features.append(hm_per_fea)

section_features = []
for i in range(len(all_train_data)):
	section_per_instance = sections_features[i]
	section_per_fea = np.array(section_per_instance[:-1])
	section_features.append(section_per_fea)

train_fea = np.concatenate((np.array(num_features),np.array(resource_features),np.array(hm_features),np.array(section_features)),axis=1)
train_label = (-1)*np.ones((len(all_train_data),))
train_label[0:len(goodware)] = 1
train_label = np.array(train_label)
###### cross-validation 

from sklearn.metrics import roc_auc_score
#auc_score_ranking = []
pos_idx = np.array(range(0,len(goodware)))
neg_idx = np.array(range(len(goodware),len(train_label)))
clf = RandomForestClassifier(n_estimators = 300)

#with open('feature_rank_result.pickle','rb') as f:
#	[rank_score_avg,rank_score_list] = pickle.load(f)

#rank_idx = np.argsort(rank_score_avg*-1.)
#fraction = [0.01,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6]
with open('feature_name_range.pickle','rb') as f:
	feature_name_range = pickle.load(f)

feature_cat_eval = []
mask_idx = np.zeros((train_fea.shape[1],))
for k in range(len(feature_name_range)):
	mask_idx = 0 * mask_idx
	start_idx = feature_name_range[k][0]
	end_idx = feature_name_range[k][1]
	name = feature_name_range[k][2]
	print(name)
	mask_idx[start_idx:(end_idx+1)] = 1.
	feature_idx = np.where(mask_idx < 1)[0]
	for iround in range(5):
		np.random.shuffle(pos_idx)
		np.random.shuffle(neg_idx)
		train_idx = pos_idx[:int(float(len(goodware)) * 0.8)].tolist() + neg_idx[:int(float(len(malware))*0.8)].tolist()
		test_idx = pos_idx[int(float(len(goodware)) * 0.8):].tolist() + neg_idx[int(float(len(malware))*0.8):].tolist()
		clf.fit(train_fea[train_idx,:][:,feature_idx],train_label[train_idx])
		decision_score = clf.predict_proba(train_fea[test_idx,:][:,feature_idx])[:,1]
		auc_score_avg += (1./5.)*roc_auc_score(train_label[test_idx],decision_score)

	#auc_score_ranking.append(auc_score_avg)
	print(auc_score_avg)
	feature_cat_eval.append((name,auc_score_avg))

with open('feature_cat_eval.pickle','w') as f:
	pickle.dump(feature_cat_eval,f)





###### 10-fold cross validation 
#[0.996284783555997, 0.9971454543042068, 0.9961996958324496, 0.9968802923142408, 0.9972881447316904,0.9971822063036102, 0.9972117974549926, 0.996646827607428, 0.9970740564371707, 0.9964977134780428]



